## Kodi addon - radio.CESNET.cz

Addon provides a seamless access to eight channels of Czech statutory public radio.
CESNET relays medium quality ogg streams for all channels and high quality FLAC streams for selected streams. 

Author is not affiliated, associated, authorized, endorsed by, maintained, or sponsored or in any way officially
connected with the Český Rozhlas, CESNET or any of their subsidiaries or their affiliates.

All product and company names are the registered trademarks of their original owners. The use of any trade name
or trademark is for identification and reference purposes only and does not imply any association with the trademark
holder of their product brand.

### Donations

* **[Buy me a coffee](https://www.buymeacoffee.com/vlna)**

### 3rd party resources

- fanart.jpg image is based on CC0 licensed image from https://www.pexels.com/photo/music-sound-audio-controls-3098/
- logos resources/logos/*.png are own work based on Peace Sans font licensed under the SIL Open Font License, Version 1.1
