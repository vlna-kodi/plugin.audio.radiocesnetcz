# -*- coding: utf-8 -*-

# Copyright (c) 2022 Vladimír Návrat
#
# This program is free software; you can redistribute it and/or modify it
# under the terms and conditions of the GNU General Public License,
# version 2, as published by the Free Software Foundation.
#
# This program is distributed in the hope it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. * See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.

import os
import sys

import requests
from urllib.parse import urlencode, parse_qs
from bs4 import BeautifulSoup

import xbmc
import xbmcgui
import xbmcplugin
import xbmcaddon

xbmc.log('radio.CESNET.cz starts ...', xbmc.LOGERROR)

color_helper = {'ČRo Radiožurnál': 'ED2E38', 'ČRo Dvojka': '85248F', 'ČRo Vltava': '00B8E0', 'ČRo D-dur': 'AB035C',
                'ČRo Jazz': '00809E', 'ČRo Radio Wave': 'CDA200', 'ČRo Plus': 'DE7008', 'ČRo Rádio Junior': '000F37'}

icon_path = os.path.join(xbmcaddon.Addon().getAddonInfo('path'), 'resources', 'logos')

# collect data
r = requests.get('http://radio.cesnet.cz/')
r.encoding = 'utf-8'

soup = BeautifulSoup(r.text, 'html.parser')
channels = []
for i in soup.find_all('div', class_='w3-row-padding'):
    if i.strong:
        streams = []
        for j in i.find_all('a', class_='w3-button'):
            codec, bitrate = [_.strip() for _ in j.text.split('/', 1)]
            streams.append({'codec': codec, 'bitrate': bitrate, 'url': j['href']})
        channels.append({'name': i.strong.text, 'streams': streams})

addon_url = sys.argv[0]
addon_handle = int(sys.argv[1])
addon_args = parse_qs(sys.argv[2][1:])

arg_channel = addon_args.get('channel', [None])[0]

xbmcplugin.setContent(addon_handle, 'songs')

if not arg_channel:
    # root menu
    for channel in channels:
        list_item = xbmcgui.ListItem(channel['name'])
        icon = os.path.join(icon_path, color_helper.get(channel['name'], '000F37') + '.png')
        list_item.setArt({'icon': icon, 'thumb': icon})
        list_item.setProperty('IsPlayable', 'true')
        xbmcplugin.addDirectoryItem(
            addon_handle, "%s?&%s" % (addon_url, urlencode({'channel': channel['name']})), list_item)

else:
    # stream selection dialog
    xbmc.executebuiltin('Playlist.Clear')
    stream = 0
    for channel in filter(lambda _: _['name'] == arg_channel, channels):
        stream = xbmcgui.Dialog().select('Select codec and bitrate',
                                         ['%s | %s' % (_['codec'], _['bitrate']) for _ in channel['streams']])
    if stream != -1:
        # play stream
        for channel in filter(lambda _: _['name'] == arg_channel, channels):
            play_url = channel['streams'][stream]['url']
            play_item = xbmcgui.ListItem(path=play_url)
            play_item.setInfo('audio', {})
            xbmc.log('radio.CESNET.cz plays %s ...' % play_url, xbmc.LOGDEBUG)
            xbmcplugin.setResolvedUrl(addon_handle, True, listitem=play_item)
    else:
        exit(False)

xbmcplugin.endOfDirectory(addon_handle)
